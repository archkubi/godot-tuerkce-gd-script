
extends Node

#bu sadece temel bakış içindir ileride detaylı olucaktır

# Godotta, 'Nodes' sahneler ve objelerdir örnek"collision,rigidbody"
# Sahneler 'root' node a sahip olmalıdır . Çünkü oluşturulan her bir node SceneTree yani sahnemizin bir child(alt sahne) nesnesidir. 
	#root node basit olarak fps karakter düşünün other node -> kinematicbody -> collisinShape
# Her bir eklenen ( attach edilen ) node , ona ( objeye ) kendine has bir davranış kazandırır. 
	#örnek küp çin fizik atamak meshInstance --> rigidbody : küp artık yerçekimi etkisi taşıycak
# (Şu anda bir node'a eklenmiş bir Script dosyasını okuyorsunuz. godot GDscript diline sahiptir )

# ÇN : "Nodes Scenes and Scripts.tscn" node içinde "Nodes Scenes and Scripts.gd" script dosyası bağlı
# ÇN : Aşağıya geçmeden önce "Nodes Scenes ve Scripts.tscn" dosyasını açınız

# Scriptler , nodelar için veri depolamak ve onlara davranış kazandırmak için kullanılır.
	#karakteri w tuşuna basıp yürütmek gibi 
# Şu anda açık olan sahne, 'F6' tuşuna basılarak veya 'Play Scene' düğmesi seçilerek başlatılabilir.
		# f6 play tuşu ana sahneyi belirlemek içindir örnek olarak oyunun başlangıç ekranı menü yani
		# f5 play scene 4. sırada sağ üst köşede ( play düğmesinin yanında )
# Ekranın alt kısmında (Output penceresinde) 'burada olanları output da görüyorsun' yazısını görüceksin
	#Output hataları görmek için önemlidir
# Aşağıdaki derslerde, Godot'nun dili olan GDScript'i nasıl kullanacağınızı öğreneceksiniz. 
# Bu kursun sonunda, sadece birkaç komut dosyası kullanarak yapılmış bir çalışma oyununa sahip olacaksınız. 
#int tam sayı , float küsüratlı sayı virgül değil nokta , str ise yazılar


var intSayi = 10   #int,float,str bunlar özel isim bu yüzden verilemez ve ı yerine i türkçe kullanma
var floatSayi = 10.5
var yaziStr = "hello f.. world"

func _ready():
	print("burada olanları output da görüyorsun")
	print(intSayi)
	print(floatSayi)
	print(yaziStr)
	anabolum() #özel fonsiyonlar kendi kendine çalışmaz

#gdscript python mantığı vardır nasıl yazdığın önemlidir
func anabolum(): #bu örnek sadece yazım biçimini anlamanız için
	if intSayi == 10:
		print("evet")
	elif intSayi == 9:
		print("hayır")
	pass # bu olmaz ise hata verir anabolum boş olduğu için
