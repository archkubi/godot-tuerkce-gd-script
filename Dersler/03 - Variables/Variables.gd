extends Node

# -------- Değişkenler (Variables , var) değiştirilebilen değerlerdir.  --------
# += : -= : *= : /= : temel matametik işlemleri
# < , >      | >=             <= | !=           , ==      |
#büyük küçük | büyük yada eşitse | eşit değilse , eşitse  |

var can = 100
var mana = 40
var stamina = 17

func _ready():
# karakter özelliklerinin ilk hali çıktısı
	print("-------- eski değerler  ---------")
	print("Can: ", can)
	print("Mana: ", mana)
	print("Stamina: ", stamina)


	can += 100
	mana -= 20
	stamina *= 7

#özellik arttırılmış hali çıktısı
	print("\n")
	print("-------- yeni değerler  ---------")
	print("Can: ", can)
	print("Mana: ", mana)
	print("Stamina: ", stamina)


# ------------- OUTPUT -------------
#	-------- eski değerler  ---------
#	Can: 100
#	Mana:  20
#	Stamina: 7
#
#
#	-------- yeni değerler  ---------
#	Can: 200
#	Mana:  20
#	Stamina: 119
# ----------------------------------
