extends Node

# Bir dizide dolaşmak istediğimizde, yineleyici değişkene kaydedebileceğimiz 2 farklı şey vardır: 
#		1) element value , Değeri
#		2) element index , Indexi

const WORDS := ["Hello", "Goodbye"]

func example_loop_1() -> void: # DEĞER öğesini yineleyicide kaydetme
	for word in WORDS:
		print(word)
		
# ------------- OUTPUT -------------
#	Hello
#	Goodbye
# ----------------------------------

# Öğe dizinini yineleyicide saklarsak, yine de o dizinde bulunan öğeyi alabiliriz. 
# Bu şekilde, dizideki her bir elemanın sadece ne olduğunu değil, aynı zamanda nerede olduğunu da biliriz. 

func example_loop_2() -> void: # INDEX öğesini yineleyicide kaydetme
	for word_index in 2:
		var word: String = WORDS[word_index]
		print(word_index, ": ", word)
		
# ------------- OUTPUT -------------
#	0: Hello
#	1: Goodbye
# ----------------------------------

func _ready() -> void:
	example_loop_1()
	example_loop_2()
