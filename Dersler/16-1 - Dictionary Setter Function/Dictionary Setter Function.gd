extends Node
# Bir sözlükte MEVCUT bir değeri ayarlamamıza/değiştirmemize izin veren bir 'ayarlayıcı' işlevi oluşturmak istiyoruz.
# (Sözlükte olmayan bir sözlük anahtarını kullanmamıza izin vermesini istemiyoruz,
# çünkü bu, sözlükte YENİ bir giriş (anahtar/değer çifti) oluşturacaktır)
const KEY := {
	name = "name",
	health = "health",
	level = "level"
}

var char_data := {
	KEY.name: "Wizard",
	KEY.health: 100,
	KEY.level: 9
}

func set_char_data_value(key: String, new_value) -> void:
	assert(char_data.has(key), str("Invalid key: ", key)) 	# Anahtarın sözlükte zaten var olduğunu iddia etmek.
	char_data[key] = new_value								# İddiamız geçerse, bu değişecek
															# 		anahtarla ilişkili mevcut değer.

func _ready() -> void:
	print(char_data)						# {health:100, level:9, name:Wizard}
	set_char_data_value(KEY.health, 500)	# Mevcut "sağlık" değerini değiştirme
	print(char_data)						# {health:500, level:9, name:Wizard}
#	set_char_data_value("power", 17)		# Bu bir hataya neden olacaktır.

# ------------- OUTPUT -------------
#	{health:100, level:9, name:Wizard}
#	{health:500, level:9, name:Wizard}
# ----------------------------------

# Artık bir sözlükte MEVCUT bir değeri güvenle değiştirebileceğimize göre, nasıl yapılacağını görelim
# sonraki derste güvenle yeni bir anahtar/değer çifti OLUŞTURUN.
