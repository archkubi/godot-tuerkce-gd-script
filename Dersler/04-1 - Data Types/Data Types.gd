extends Node
# Godot'ta birçok veri türü vardır, bazı temel olanlar: 

# ---- (Tamsayı) Integer (int) - tam sayı (ondalık basamak yok) (pozitif veya negatif)  ----
var tamsayimiz = 123

# ---- Float (float) - ondalık basamaklı sayı (pozitif veya negatif)  ----
var ondalikSayimiz = 123.456

# ---- String ( Metin Dizesi ) - 0 veya daha fazla metin karakteri  ----
var metinimiz = "hello"
var farkliMetinimiz = 'goodbye'
var bosMetinimiz = ""
var butunMetin = """
	   -------------          archkubi@GnuChanOS 
	-------------------       ------------------ 
  ------     ---------        OS: Debian GNU/Linux 11 (bullseye) x86_64 
 -----              -----     Host: CASPER NIRVANA DESKTOP 1.0 
-----       ---------------   Kernel: 5.10.0-12-amd64 
-----     ----------------    Uptime: 6 hours, 27 mins 
 ---      -----------------   Packages: 3104 (dpkg) 
 ---      ----------------    Shell: bash 5.1.4 
 ---      --------------      Resolution: 1024x768 
 ------- ------------         WM: i3 
 -----     -----              Theme: gnuchanos-theme [GTK2/3] 
  ----                        Icons: gnuchanos-icons [GTK2/3] 
   -----                      Terminal: cool-retro-term 
	 -----                    CPU: Intel i3-4150 (4) @ 3.500GHz 
	   ------                 GPU: Intel 4th Generation Core Processor Family 
		  -------             Memory: 2889MiB / 3801MiB 
"""

# ---- Boolean (bool) - doğru veya yanlış ----
var mantikDegeri = true
var olumsuzMantikDegeri = false

# ---- Vector2 (Vector2) - 2 sayı kümesi (int veya float) (float değere dönüştürülecektir)  ----
# Iki eksenli bir uzayda nokta belirlemek için kullanılabilir. 
var vectorumuz = Vector2(20,50.3)

# ---- Vector3 (Vector3) - 3 sayı kümesi (int veya float) (float değere dönüştürülecektir) ----
# Üç eksenli bir uzayda nokta belirlemek için kullanılabilir. 
var vectorumuzUcEksenli = Vector3(20, 50.3, -7.4)

# ---- Renk - Kırmızı, Yeşil, Mavi ve Alfa (opaklık) kanalının değerleri  ----
var renk = Color.red
var baskaBirRenk = Color(1, 0, 0, 1)

# ---- liste - değişken listesi (elemanlar)  ----
var liste = [1, 2, 3, 4, 5,]
var kompleksBirliste = [1, -0.2, "C", true, false, Color.green,]
var bozliste = []

# ---- Sözlük - anahtar/değer çiftlerinin listesi ----
# { "veri1" : "veri1 in karşılığı" , "veri2" : "veri2 in karşılığı"}
var sozluk = {"veri": 100, "veri_2": 55}
var sozluk_2 = {
	"Meyve": "Armut",
	"Sebze": "Domates",
}
var sozluk_3 = {
	favori_yemek = "pizza",
	favori_hayvan = "kopek"
}
var my_empty_dictionary = {}

func _ready():
	print("metinimiz: ", metinimiz)
	print("farkliMetinimiz: ", farkliMetinimiz)
	print("butunMetin: ", butunMetin)

	print("---------------------")

	print("mantikDegeri: ", mantikDegeri)
	print("olumsuzMantikDegeri: ", olumsuzMantikDegeri)

	print("---------------------")

	print("vectorumuz: ", vectorumuz)

	print("---------------------")

	print("renk: ", renk)
	print("baskaBirRenk: ", baskaBirRenk)

	print("---------------------")

	print("liste: ", liste)
	print("kompleksBirliste: ", kompleksBirliste)
	print("liste: ", kompleksBirliste[0]) #listeden seçme
	print("---------------------")

	print("sözlük: ", sozluk)
	print("sözlük 2: ", sozluk_2)
	print("sözlük 3: ", sozluk_3)

	print("---------------------")

	print("sözlükden seçim")
	print("veri: ", sozluk["veri"])
	print("veri 2: ", sozluk["veri_2"])
	print("sozluk 2: ", sozluk_2["Meyve"])
	print("favori_yemek: ", sozluk_3["favori_yemek"])
	print("---------------------")
