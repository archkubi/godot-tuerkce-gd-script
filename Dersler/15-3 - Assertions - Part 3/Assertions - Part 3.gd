extends Node
# Merak ediyor olabilirsiniz: "Bir ifadenin yanlış olduğunu iddia etmenin anlamı nedir?"
# Bu aslında göründüğünden çok daha faydalı.

# Örneğin, eklemeden önce bir elemanın zaten bir dizide OLMADIĞINI söyleyebiliriz.
# Bu, bir dizide asla yinelenen bir değere sahip olmamamızı sağlayacaktır.

# Yanlış bir iddia sözlükler için de faydalı olabilir.
# Sözlüğe yeni bir giriş (anahtar/değer çifti) eklemek istediğimizi varsayalım:
# Kullanmak istediğimiz anahtar sözlükte zaten varsa, yanlışlıkla
# mevcut bir girişi değiştirmek (yenisini yapmak yerine).
# Bunu yapmadan önce anahtarın sözlükte OLMADIĞINDAN emin olmak için bir iddia kullanabiliriz.


# Ayrıca, bir tamsayı döndüren bazı işlevler, bir şeylerin yanlış gittiğini belirtmek için -1 değerini döndürür.
# Her şeyin yolunda gittiğinden emin olmak için, döndürülen tamsayının -1 OLMADIĞINI basitçe söyleyebiliriz.

# Diğer durumlarda, bir ifadenin doğru yerine yanlış olup olmadığını kontrol etmek daha kolay olabilir.
# Örneğin, daha sonra değiştirmeyi düşündüğünüz varsayılan değeri olan bir değişkeniniz olabilir.
# Kodunuzun sonraki bir noktasında, değişkenin olduğundan emin olmak için bir iddia kullanabilirsiniz.
# hala varsayılan değerine sahip DEĞİLDİR (çünkü varsa, değiştirmeyi unuttuğunuz anlamına gelir).

# Bunlar neden yanlış bir iddia kullanmak isteyebileceğinize dair sadece birkaç örnek.
