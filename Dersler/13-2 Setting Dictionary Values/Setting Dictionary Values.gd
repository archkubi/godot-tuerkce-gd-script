extends Node

# sözlükten değer almanın 3 yolunu inceleyelim

var char_info := {
	name = "Wizard",
	health = 100,
	level = 9,
}

var char_name: String = char_info["name"]				# methot 1
var char_health: int = char_info.health					# methot 2
var char_level: int = char_info.values()[2]				# methot 3

# değer atayarak 1 ve 2 yi kullandık fakat 3 farklıydı.
func _ready() -> void:
	print(char_info)									# {health:100, level:9, name:Wizard}
	
	char_info["name"] = "Knight"						# methot 1
	char_info.health = 120								# methot 2
	char_info.values()[2] = 13							# methot 3 (işe yaramadı)
	print(char_info)									# {health:120, level:9, name:Knight} ("level" değeri değişmedi)
	print(char_info.values())							# [Knight, 120, 9]

#	Yöntem 3 ayarlayıcı olarak çalışmayacaktır, çünkü aslında yaptığımız ayar yapmaktır.
#			"values()" işlevinin oluşturduğu ve döndürdüğü yinelenen dizideki bir öğedir.
#			Sözlüğün kendisindeki gerçek değeri değiştirmiyoruz.
	var duplicate_values := char_info.values()			# .values() benzersiz, yinelenen bir dizi döndürür
	duplicate_values[2] = 77							# yalnızca yinelenen dizide bir değer ayarlar
	print(duplicate_values)								# [Knight, 120, 77] (değerleri çiftler)
	print(char_info.values())							# [Knight, 120, 9] (original değerler) (değişmedi)
