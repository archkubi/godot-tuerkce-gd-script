extends Node
# Bir dizideki belirli bir elemanın değerini değiştirmek için aynı şeyi yaparız
#		ardından bir eşittir işareti ve ardından yeni değer yazın
var my_array := ["dog", "fish", "cat"]

func _ready() -> void:
	print(my_array) 									#	[dog, fish, cat]
	my_array[0] = "snake"								#	(0. indexi "snake" yaptık)
	print(my_array)										#	[snake, fish, cat]
#	We can do this using a for-loop as well.
	for index in my_array.size():						#	iterasyon değişkenini kayıt aldık
		my_array[index] = "duck"						# 	(arraydaki tüm elementler "duck" oldu)

	print(my_array)										#	[duck, duck, duck]

#	Bir döngü kullanarak bir diziden diğerine değerler ekleyebiliriz
	var animal_names := ["frog", "mouse", "bird"]		#	Yeni bir dizi oluşturma

	for animal_name in animal_names:					#	yeni hayvan adları yineleyiciden tanımlanacak
		my_array.append(animal_name)					#	yeni hayvan isimleri 'my_array'a ekleniyor

	print(my_array)										#	[duck, duck, duck, frog, mouse, bird]
	var food_names := ["pizza", "cake", "cookies"]

	for index in food_names.size():						#	Dizini yineleyici değişkene kaydetme
		var food_name: String = food_names[index]		#	'food_names' elementini al
		my_array[index] = food_name						#	'my_array' elementlerini ilk 3 e yaz

	print(my_array)										#	[pizza, cake, cookies, frog, mouse, bird]

	# İki diziyi birleştirmek (birleştirmek) için += operatörünü de kullanabiliriz.
	var thing_names := ["book", "tree"]					#	dizi oluştu
	my_array += thing_names								#	'thing_names'ten 'my_array'e tüm elementler eklendi
	print(my_array)										# 	[pizza, cake, cookies, frog, mouse, bird, book, tree]
