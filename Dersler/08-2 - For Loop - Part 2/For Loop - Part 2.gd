extends Node
# Döngümüzü 'N' kez çalıştırmak istiyorsak, yinelenebilir olarak bir sayı kullanabiliriz.

func my_number_loop() -> void:
	for number in 3:
		print(number)
		
# ------------- OUTPUT -------------
#	0
#	1
#	2
# ----------------------------------

# Döngü, yineleyici değişkeni tam olarak 3 kez yazdırdı. 

# Yazdırılan sayıların beklediğiniz gibi 1-3 değil 0-2 olduğunu unutmayın. 
# Aşağıdaki işlevler, yukarıdaki işlevle aynı şeyi yapacaktır.

func my_range_loop() -> void:
	for number in range(3):
		print(number)

func my_array_loop() -> void:
	for number in [0, 1, 2]:
		print(number)

# 'range(N)' işlevi, 0 ile (N - 1) arasında bir sayı dizisi döndürür.
# Döndürülen dizinin 0 ile başlamasının nedeni,
#		dizi 'indexes , indeksleri' hakkında bilgi edinin (bir sonraki derste).

func _ready() -> void:
	my_number_loop()
