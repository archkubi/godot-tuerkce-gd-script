extends Node

# Sonunda 2 değeri karşılaştırmak isteyeceğiz.
# Örneğin, karakterimizin hayatta olup olmadığını kontrol etmek için sağlığını 0 ile karşılaştırabiliriz.

# Sağlığı 0'dan büyükse yaşıyor demektir.
# Aksi halde ölmüş olmalı.

# 2 değeri karşılaştırmak için kullanabileceğimiz karşılaştırma operatörleri:
# < (daha az)
# > (büyüktür)
# == (eşittir) *** Burada dikkatli olun. ('=' ve '==' farklı şeyler yapar)
# != (eşit değil)
# >= (büyüktür veya eşittir)
# <= (küçük veya eşit)

var health := 20

func _ready() -> void:
	print(health < 0)		# False
	print(health > 0)		# True
	print(health == 0)		# False							'==' eşitlik operatörüdür
#	print(health = 0)		# (Hata: Beklenmeyen atama)      '=' atama operatörüdür
	print(health != 0)		# True
	print(health >= 0)		# True
	print(health <= 0)		# False

