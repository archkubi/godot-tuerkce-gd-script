extends Node
# Çoğu zaman, belirli şeylerin yalnızca belirli koşullar altında olmasını isteriz.
# Örneğin, karakterimiz ne zaman canını kaybetse, hala hayatta olup olmadığını kontrol etmemiz gerekiyor.
# Hâlâ hayattalarsa, oyunu oynamaya devam edebiliriz.
# Aksi takdirde Game Over ekranını göstermemiz gerekiyor.

# if-ifadeleri bunun içindir.
# 'if' anahtar sözcüğünü ve ardından değerlendirmek istediğimiz ifadeyi kullanarak bir if ifadesi oluşturabiliriz.

var health := 20
var damage_amount := 8

func damage_player() -> void:
	health -= damage_amount
	print("You took ", damage_amount, " damage.", " (health: ", health, ")")

	if health <= 0:				# if ifadesi (ifadesi) doğruysa, altındaki kod bloğu yürütülür.
		print("You died.")		# (if ifadesinin kod bloğunun ilk satırı)
		print("Game Over.")		# (if ifadesinin kod bloğunun ikinci satırı)


func _ready() -> void:
	damage_player()		# 8 damage. (health: 12)
	damage_player()		# 8 damage. (health: 4)
	damage_player()		# 8 damage. (health: -4)
						# öldün .
						# Oyun bitti .

# Bir sonraki derste if ifadesi (ifadesi) false olduğunda kodun nasıl çalıştırılacağını öğreneceğiz.
