extends Node
# Statik olarak bir değişken yazarken, Godot çıkarım yapabilir (otomatik olarak çözebilir) 

# Godot veri tipini çıkardığı zaman, onu açıkça yazmamız gerekmez.

var my_var_A: int = 100
#	Bildirilen veri türü. Godot'ya bu değişkenin bir int olduğunu açıkça söyledik.

var my_var_B := 100
#	Çıkarım yapılan veri türü. Godot, bu değişkenin bir int olduğu sonucunu çıkardı. 
#	(Godot, 100'ün bir tam sayı olduğunu biliyor, bu yüzden bunu söylememize gerek yok.)

# Varsayılan işlev argümanlarında tür çıkarımını da kullanabiliriz
func get_squared_number_or_zero(number := 0) -> int:
	return number * number

func _ready() -> void:
	var squared_number := get_squared_number_or_zero(5) # 'squared_number' şimdi statik olarak int olarak yazılmıştır
	
# 	Çünkü dönüş değeri 'get_squared_number_or_zero' statik olarak int olarak yazılır ,
#		tür çıkarımını kullanarak statik olarak 'squared_number'ı int olarak yazabildik.

#	Godot'nun bunu çıkarmasına izin vermek yerine, türü her zaman açıkça belirtebilirsiniz, 
#		bu, kodu sizin için daha net hale getirirebilir.

#	Ancak, türüne uymayan bir değişkene değer atamaya çalışırsanız bir hata alırsınız. 

#	Tür çıkarımı, daha az gereksiz kod yazmanıza olanak tanır (muhtemelen netlik pahasına olsa da). 

