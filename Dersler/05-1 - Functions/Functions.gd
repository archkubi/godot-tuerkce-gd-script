extends Node
# Fonksiyonlar ( işlevler ) , onu çağırarak  çalıştırılabilen kod bloklarıdır .

# Godot, otomatik olarak 'çağrılan' (yürütülen) bazı motorda gömülü işlevlere sahiptir. 
# The '_ready()' işlevi bunlardan biridir. Bir 'node' kurulumu bittiğinde çağrılır. 

# 'print()' Godot'ta yerleşik başka bir fonksiyondur.
# 		Editörün altındaki Godot'nun 'Output' panelinde metin yazacaktır. 

# Değişkenler ve sabitler ile yaptığımız gibi kendi fonksiyonlarımızı oluşturabiliriz. 

var skorlar: int = 20

func skor_yukselt():
	skorlar += 1
	print("skor arttırıldı.")

func _ready():
	print(skorlar)		# 20
	skor_yukselt()
	print(skorlar)		# 21
	skor_yukselt()
	print(skorlar)		# 22

# altındaki her satırın nasıl olduğuna dikkat edin. 'skor_yukselt()' fonksiyonu ve '_ready()' işlevi zincirlemedir.
# Bu zincirleme akış , Godot'a hangi kodun birlikte gruplandığını söyleyen bir "kod bloğu" yaratır. 
# Bu, girintinin aslında kodumuz üzerinde bir etkisi olduğu ve yalnızca görsel bir yardım olmadığı anlamına gelir.

# Bazı diller, bir kod bloğunun nerede başlayıp nerede bittiğini belirtmek için köşeli ayraçlar ({}) kullanır. 
#	ancak GDScript'te kod blokları girinti ("tabs" yani sekmeler) kullanılarak tanımlanır.

# ------------- OUTPUT -------------
# 20
# skor arttırıldı.
# 21
# skor arttırıldı.
# 22
# ----------------------------------
