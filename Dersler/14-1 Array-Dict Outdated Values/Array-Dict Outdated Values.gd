extends Node

# Dizileri ve sözlükleri kullanmanın olası bir tuzağının farkında olmak önemlidir: güncel olmayan değerler.
# Bir değişkeni bir diziye veya sözlüğe kaydedersek, değişkeni değiştiririz,
# dizi/sözlük bu değişikliği yansıtmaz.
# Dizinin/sözlüğün içindeki değeri doğrudan değiştirmemiz gerekecek.

var health := 100
var level := 9

var char_stats_array := [health, level]			# değişkenleri dizi öğeleri olarak kaydetmek

var char_stats_dict:= {							# değişkenleri sözlük değerleri olarak kaydetmek
	"health": health,
	"level": level
}

func _ready() -> void:
	print(char_stats_array)						# [100, 9]
	print(char_stats_dict)						# {health:100, level:9}

	health *= 2									# 'health' değişken çiftlendi
	level *= 2									# 'level' değişken çiftlendi

	print(health)								# 200 ('health' çiftlendi)
	print(level)								# 18 ('level' çftlendi)

	print(char_stats_array)						# [100, 9] (dizi elemanları çiftlenmedi)
	print(char_stats_dict)						# {health:100, level:9} (sözlük elemanları çftlenmedi
	
# 	Bir sonraki derste bu sorunun çözümünü tartışacağız.
	

