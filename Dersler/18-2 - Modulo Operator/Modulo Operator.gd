extends Node
#  Modulo (mutlak değer) operatörü (%) 2 sayının bölme kalanını döndürür.
# Diyelim ki şöyle bir bölme denklemimiz var:
#			6 / 3 = ?

# Bu denklem için uzun bölme diyagramı şöyle görünür:
#				 ?
#			   -----
#			3 )  6

# Denklemi çözersek, diyagramımız şöyle görünecektir:

#				 2				(6 nın içinde 3 ten 2 tane var)
#			   -----
#			3 )  6
#			   - 6
#			    ___
#				 0				(6'yı 3'e böldükten sonra kalan kalmaz)

# Bu bölümden (0) sonra kalan, modulo operatörümüzün (%) döndürdüğü değerdir. (6 % 3 = 0)
# Tam bölünemeyen 2 sayı varsa, modulo işlemimiz 0 döndürmez.
#			5 / 2 = ?
#-------------------------------------------------------------------------------------------
#				 ?
#			   -----
#			2 )  5
#-------------------------------------------------------------------------------------------
#				 2.x			(5 in içinde 2 den 2 tane vardır , ekstra olarak elimizde 1 vardır)
#			   -----
#			2 )  5
#			   - 4
#			    ___
#				 1				(1 kalandır)			(5 % 2 = 1)

func _ready() -> void:
	print(6 % 3)	# 0
	print(5 % 2)	# 1

