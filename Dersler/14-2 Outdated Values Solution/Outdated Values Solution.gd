extends Node

# Bir değişkenin değerini bir diziye veya sözlüğe kaydettiğimizde, değişkeni değiştirin,
# dizideki/sözlükteki değer, değişkenin mevcut değeriyle eşleşmeyecek.

# Bu sorunun en iyi çözümü değişkenlerin YERİNE bir sözlük kullanmaktır.
# Değerlerin saklandığı sadece 1 yerimiz varsa, bunlar eski olamaz.

# Sadece string anahtarlarını saklayan bir sözlük oluşturabiliriz. Bu dizeler değişken adlarını "simüle edecek".
const KEY := {			# name değişkeni değiştirilemediğinden bu dict bir const olmalıdır.
	name = "name",
	health = "health",
	level = "level"				
}



# Şimdi değerleri gerçekten saklayacak bir sözlük oluşturabiliriz.
var char_data := {
	KEY.name: "Wizard",			#Bir '.' kullandığımız için bu sözlükte iki nokta üst üste işareti kullanmalıyız.
	KEY.health: 100,
	KEY.level: 9,
}

# Artık değerleri normalde alacağımız gibi alabiliriz ve endişelenmemiz gereken herhangi bir değişken uyumsuzluğu yok.
# Ayrıca, artık 'char_data' her yazdığımızda tüm bu "değişken adları" için otomatik tamamlama alıyoruz.
# Bu, verilerimizi gruplamak ve düzenlemek için harika bir yoldur (ayrı değişkenlere sahip olmanın aksine).
func _ready() -> void:
	print(char_data.name)		# Wizard
	print(char_data.health)		# 100
	print(char_data.level)		# 9
	
# Aşağıdaki derslerde sözlük değerlerini ayarlarken kolayca yapılan bir hatanın çözümünü tartışacağız.
