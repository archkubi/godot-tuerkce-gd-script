extends Node

# Döngüler, belirli bir koşul sağlanana kadar kodun bir bölümünü tekrarlamamıza izin verir. 
# Kodu belirli sayıda tekrarlamak için bir 'for döngüsü' kullanılabilir.

var listemiz := ["A", "B", "C", "D"]

func harfleri_dondur() -> void:
	for deger in listemiz:
		print(deger)

# ------------- OUTPUT -------------
#	A
#	B
#	C
#	D
# ----------------------------------

# Bir dizide dolaşırken (yukarıda gösterildiği gibi), her dizi elemanı bir 
#		geçici değişken ('deger' adını verdik).
#		(Bu geçici değişkene 'yineleyici değişken , iterator variable' denir .)

# Daha sonra her döngü yinelemesinde (adım/döngü) yineleyici değişkeni yazdırdık. 

# Bir döngünün üzerinde yinelendiği (adım adım ilerlediği) şeye 'iterable , itere edilebilir yani , yinelenebilir' denir. 
# Bu örnekte, yinelenebilir, 'listemiz' dizisiydi.

# ÇN: Burada itere edilen listemizdeki A,B,C,D gibi düzenli ilerleyen harfler değildir .
	#ÇN: listeler A , B , C diye biz belirtirken ,
	#ÇN:          0 , 1 , 2 olarak index değerleri atamaktadır 
	#ÇN:		 her döngüde bu değer arttığından A,B,C,F diye ilerlense bile
	#ÇN:           indexi  0,1,2,3 atanacaktır ve A,B,C,F çıktısı alabiliriz
	#ÇN: dizi 'indexes , indeksleri' hakkında bilgi edinin (iki sonraki derste).
func _ready() -> void:
	harfleri_dondur()




